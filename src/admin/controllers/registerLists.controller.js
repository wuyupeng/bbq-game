/**
 * registerlists Controller
 */
angular.module('controllers').controller('registerLists', ['$scope', '$http', 'account',
  function ($scope, $http, account) {
    'use strict';

    /**
     * 初始化变量
     */
    $scope.lists = [];

    /**
     * 获取角色列表
     */
    $http.get('/api/registerLists')
      .then(function (data) {
        if (data.status === 200) {
          $scope.lists = data.data;
        }
      });

  }
]);
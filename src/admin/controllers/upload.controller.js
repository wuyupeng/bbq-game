/**
 * upload Controller
 */
angular.module('controllers').controller('upload', ['$scope', '$http',
  function ($scope, $http, account) {
    var page = window.location.search ? Number(window.location.search.split('page=')[1]) : 1;
    'use strict';

    /**
     * 初始化变量
     */
    $scope.lists = [];
    $scope.blob = '';
    $scope.fileName = '';
    $scope.countPage = []
    $('.upload-tips').hide();
    $('.upload-tips-error').hide();
    getRanks(page);
    $("#file").change(function (e) {
      $scope.blob = e.target.files[0];
      $scope.fileName = $scope.blob.name;
      if ($scope.blob || scope.fileName) {
        var formdata = new FormData();
        formdata.append("csv", $scope.blob);
        $.ajax({
          url: '/api/upload',
          data: formdata,
          method: 'post',
          processData: false,
          contentType: false,
          success: function (data) {
            if (data.success) {
              $('.upload-tips').show();
              $('.upload-tips-error').hide();
              setTimeout(function () {
                $('.upload-tips').hide();
                window.location.reload();
              }, 2000)
            }
          },
          error: function (err) {
            $('.upload-tips').hide();
            $('.upload-tips-error').show();
            setTimeout(function () {
              $('.upload-tips-error').hide();
            }, 4000)
          }
        })
      }
    })



    /**
     * 获取排行榜
     */
    function getRanks(size) {
      $http.get('/api/upload?page=' + size)
        .then(function (data) {
          if (data.status === 200) {
            $scope.lists = data.data.lists;
            $scope.countPage = data.data.countPage;
          }
        });
    }
  }
]);
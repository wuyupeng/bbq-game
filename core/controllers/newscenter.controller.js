var async = require('async');
var _ = require('lodash');
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');
var listsService = require('../services/lists.service');
var modelService = require('../services/models.service');
var featureService = require('../services/features.service')


/**
 * 新闻中心页
 * @param {Object} req
 * @param {Object} res
 */
module.exports = function (req, res) {
  var map = {
    tradingtips: '交易心得',
    story: '盘手故事',
    interview: '导师访谈',
    report: '大赛播报',
    all: '全部'
  }
  var center;
  var type;
  if (!req.query.type) {
    type = 'all'
  } else if (req.query.type && !map[req.query.type]) {
    return res.status(400).end()
  } else {
    center = map[req.query.type];
    type = req.query.type
  }
  async.parallel({
    siteInfo: siteInfoService.get,
    navigation: function (callback) {
      categoriesService.allNavigation({
        current: '/news-center'
      }, callback)
    },
    models: function (callback) {
      if (type === 'all') {
        featureService.all(function (err, data) {
          if (!err) callback(null, _.slice(data, 0))
        })
      } else {
        async.waterfall([
          function (callback) {
            modelService.query({
              query: {
                name: center
              }
            }, callback)
          },
          function (models, callback) {
            if (_.size(models) === 0) callback(null, [])
            var _id = _.get(_.first(models), '_id');
            featureService.selectOne({
              _id
            }, callback)
          }
        ], callback)
      }
    }
  }, function (err, results) {
    var models = results.models;
    models = _.map(models, function (value, key) {
      var videoUrl = ''
      if (value.url) {
        var reg = /id_(\w+)==/g
        reg.exec(value.url);
        videoUrl = `http://player.youku.com/player.php/sid/${RegExp.$1}==/v.swf`
      }
      return {
        title: value.title,
        imgsrc: _.get(value, 'thumbnail.src', ''),
        author: _.get(value, 'extensions.author', ''),
        time: getDateTime(_.get(value, 'thumbnail.date', '')),
        id: value._id,
        video: value.url ? true : false,
        videoUrl: videoUrl
      }
    })
    if (err) return res.status(500).end();
    var objT = {}
    objT[type] = true;
    var noL = false
    if (_.size(models) < 1) {
      noL = true
    }
    res.render('news', {
      layout: 'layout-default',
      siteInfo: results.siteInfo,
      navigation: results.navigation,
      models: models,
      type: objT,
      noL: noL
    });
  });
};


function getDateTime(str) {
  var time = str ? new Date(str) : new Date();
  return time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate()
}
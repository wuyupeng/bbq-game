var async = require('async');
var _ = require('lodash');
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');
var listsService = require('../services/lists.service');
var modelService = require('../services/models.service');
var featureService = require('../services/features.service')
/**
 * 大赛规则页
 * @param {Object} req
 * @param {Object} res
 */
module.exports = function (req, res) {
  async.parallel({
    siteInfo: siteInfoService.get,
    navigation: function (callback) {
      categoriesService.allNavigation({
        current: '/rules'
      }, callback)
    },
  }, function (err, results) {
    if (err) return res.status(500).end();
    res.render('rules', {
      layout: 'layout-default',
      siteInfo: results.siteInfo,
      navigation: results.navigation,
    });
  });
};
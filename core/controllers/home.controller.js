var async = require('async');
var _ = require('lodash');
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');
var listsService = require('../services/lists.service');
var modelService = require('../services/models.service');
var featureService = require('../services/features.service')
var uploadService = require('../services/upload.service');

/**
 * 首页
 * @param {Object} req
 * @param {Object} res
 */
module.exports = function (req, res) {
  async.parallel({
    siteInfo: siteInfoService.get,
    // navigation: function (callback) {
    //   categoriesService.navigation({ current: '/' }, callback);
    // },
    navigation: function (callback) {
      categoriesService.allNavigation({
        current: '/'
      }, callback)
    },
    lists: function (callback) {
      uploadService.get({
        pageSize: 5,
        currentPage: 1
      }, function (err, data) {
        if (err && data.length < 1) callback(null, []);
        callback(null, data)
      })
    },
    models: function (callback) {
      featureService.all(function (err, data) {
        if (data && _.size(data) > 0) {
          data = _.map(data, function (value, key) {
            var videoUrl = ''
            if (value.url) {
              var reg = /id_(\w+)==/g
              reg.exec(value.url);
              videoUrl = `http://player.youku.com/player.php/sid/${RegExp.$1}==/v.swf`
            }
            return {
              title: value.title,
              imgsrc: _.get(value, 'thumbnail.src', ''),
              author: _.get(value, 'extensions.author', ''),
              time: getDateTime(_.get(value, 'thumbnail.date', '')),
              id: value._id,
              video: value.url ? true : false,
              videoUrl: videoUrl
            }
          })
          modelsL = {
            noL: false,
            lists: _.slice(data, 0, 8)
          }
        } else {
          modelsL = {
            noL: true,
            lists: []
          }
        }
        callback(null, modelsL)
      })
    }
  }, function (err, results) {
    if (err) return res.status(500).end();
    res.render('home', {
      layout: 'layout-default',
      siteInfo: results.siteInfo,
      navigation: results.navigation,
      models: results.models,
      lists: results.lists,
      firstData: _.size(results.models.lists) > 0 ? results.models.lists[0] : undefined
    });
  });
};

function getDateTime(str) {
  var time = str ? new Date(str) : new Date();
  return time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate()
}
var async = require('async');
var _ = require('lodash');
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');
var listsService = require('../services/lists.service');
var modelService = require('../services/models.service');
var featureService = require('../services/features.service')
/**
 * 帮助中心页
 * @param {Object} req
 * @param {Object} res
 */
module.exports = function (req, res) {
  async.parallel({
    siteInfo: siteInfoService.get,
    navigation: function (callback) {
      categoriesService.allNavigation({
        current: '/help-center'
      }, callback)
    },
  }, function (err, results) {
    if (err) return res.status(500).end();
    res.render('help', {
      layout: 'layout-default',
      siteInfo: results.siteInfo,
      navigation: results.navigation,
    });
  });
};
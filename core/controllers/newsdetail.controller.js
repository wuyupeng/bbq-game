var async = require('async');
var _ = require('lodash');
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');
var listsService = require('../services/lists.service');
var modelService = require('../services/models.service');
var featureService = require('../services/features.service')


/**
 * 新闻中心页
 * @param {Object} req
 * @param {Object} res
 */
module.exports = function (req, res) {
  var _id = req.params.id;
  if (!_id) return res.status(404).end();
  var newsData;
  async.waterfall([
    function (callback) {
      featureService.findOne({
        _id
      }, function (err, data) {
        if (err || !data) return res.status(404).end();
        callback(null, data)
      })
    },
    function (data, callback) {
      newsData = _.first(data);
      async.parallel({
        siteInfo: siteInfoService.get,
        navigation: function (callback) {
          categoriesService.allNavigation({
            current: '/news-center'
          }, callback)
        },
      }, callback)
    }
  ], function (err, results) {
    var videoUrl = ''
    if (newsData.url) {
      var reg = /id_(\w+)==/g
      reg.exec(newsData.url);
      videoUrl = `http://player.youku.com/player.php/sid/${RegExp.$1}==/v.swf`
    }
    var data = {
      title: newsData.title,
      imgsrc: _.get(newsData, 'thumbnail.src', ''),
      author: _.get(newsData, 'extensions.author', ''),
      time: getDateTime(_.get(newsData, 'thumbnail.date', '')),
      id: newsData._id,
      content: _.get(newsData, 'extensions.content', ''),
      video: newsData.url ? true : false,
      videoUrl: videoUrl
    }
    res.render('newsdetail', {
      layout: 'layout-default',
      siteInfo: results.siteInfo,
      navigation: results.navigation,
      news: data,
    });
  })
};



function getDateTime(str) {
  var time = str ? new Date(str) : new Date();
  return time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate()
}
var async = require('async');
var _ = require('lodash');
var siteInfoService = require('../services/site-info.service');
var categoriesService = require('../services/categories.service');
var listsService = require('../services/lists.service');
var modelService = require('../services/models.service');
var featureService = require('../services/features.service')
var uploadService = require('../services/upload.service');

/**
 * 排行榜页
 * @param {Object} req
 * @param {Object} res
 */
module.exports = function (req, res) {
  var currentPage = parseInt(req.query.page, 10) || 1;
  var pageSize = 10;
  var count;
  async.parallel({
    siteInfo: siteInfoService.get,
    navigation: function (callback) {
      categoriesService.allNavigation({
        current: '/rank'
      }, callback)
    },
    count: function (callback) {
      uploadService.getCount(function (err, data) {
        count = data;
        callback(null, data)
      })
    },
    lists: function(callback) {
      if((currentPage - 1) * pageSize > count) {
        return res.redirect('/rank')
      }
      currentPage = Math.max(1, Math.ceil(currentPage));
      var options = {
        pageSize,
        currentPage
      }
      uploadService.get(options, function(err, data) {
        if(err && data.length < 1) callback(null, []) 
        callback(null, data)
      })
    }
  }, function (err, results) {
    if (err) return res.status(500).end();
    var countPage = _.map(new Array(Math.ceil(count/pageSize)), (v, i) => {
      return {
        num: i + 1
      }
    })
    res.render('rank', {
      layout: 'layout-default',
      siteInfo: results.siteInfo,
      navigation: results.navigation,
      lists: results.lists,
      count: countPage,
      currentPage: currentPage
    });
  });
};
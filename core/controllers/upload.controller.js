var _ = require('lodash');
var logger = require('../../lib/logger.lib');
var uploadService = require('../services/upload.service');
var formidable = require('formidable')
var csv = require('node-csv').createParser();
// var csv = require('csv');
var fs = require('fs')
// console.log(csv())
var form = new formidable.IncomingForm();
var async = require('async');
var iconvLite = require('iconv-lite');

function camelCase(string) {
  // Support: IE9-11+
  return string.replace(/_([a-z])/g, function (all, letter) {
    return letter.toUpperCase();
  });
}


/**
 * 上传成绩
 * @param {Object} req
 * @param {Object} res
 */
exports.get = function (req, res) {
  var currentPage = parseInt(req.query.page, 10) || 1;
  var pageSize = 40;
  var count
  async.waterfall([
    function (callback) {
      uploadService.getCount(function (err, data) {
        count = data;
        callback(null, data)
      })
    },
    function (count, callback) {
      if ((currentPage - 1) * pageSize > count) {
        return res.redirect('/rank')
      }
      currentPage = Math.max(1, Math.ceil(currentPage));
      var options = {
        pageSize,
        currentPage
      }
      uploadService.get(options, function (err, data) {
        if (err && data.length < 1) callback(null, [])
        callback(null, data)
      })
    }
  ], function(err, data) {
    if(!err) {
      var countPage = _.map(new Array(Math.ceil(count / pageSize)), (v, i) => {
        return {
          num: i + 1
        }
      })
      var obj = {
        lists: data,
        countPage
      }
      res.status(200).send(obj)
    }
  })
}


exports.post = function (req, res) {
  async.waterfall([
    function (callback) {
      form.parse(req, function (err, fields, files) {
        var path = files.csv.path;
        if (!path) callback({
          type: 'page',
          error: 'csv解析错误'
        })
        fs.readFile(path, function (err, data) {
          csv.parse(iconvLite.decode(data, 'gbk'), (err, data) => {
            if (err) callback({
              type: 'page',
              error: 'csv解析错误'
            })
            var property = _.first(data);
            var lists = _.slice(data, 1);
            var ranks = _.map(lists, (list) => {
              var obj = {}
              _.forEach(list, (v, k) => {
                obj[camelCase(property[k])] = v;
              })
              return obj
            })
            var ranksData = _.map(ranks, (value, index) => {
              value.lastModifiedDate = files.csv.lastModifiedDate;
              value.type = 'ranks';
              value.rankIndex = Number(value.rankIndex);
              return value
            })
            callback(null, ranksData)
          })
        })
      })
    },
    function (data, callback) {
      uploadService.save(data, function (err, data) {
        if (err) callback(err)
        callback(null)
      })
    }
  ], function (err, data) {
    if (err) {
      logger.system().error(__filename, err)
      res.status(400).end()
    }
    res.send({
      success: true
    })
  })
}
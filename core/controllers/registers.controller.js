var _ = require('lodash');
var logger = require('../../lib/logger.lib');
var registerService = require('../services/register.service');

/**
 * 注册
 * @param {Object} req
 * @param {Object} res
 */
exports.post = function (req, res) {
  req.checkBody({
    'exchange': {
      isString: {
        errorMessage: 'exchange必填'
      }
    },
    'name': {
      isString: {
        errorMessage: 'name必填'
      }
    },
  });
  if (req.validationErrors()) {
    logger.system().error(__filename, '参数验证失败', req.validationErrors());
    return res.status(400).end();
  }
  registerService.save(req.body, function (err, data) {
    if (err) {
      logger[err.type]().error(__filename, err);
      return res.status(500).end();
    }
    res.status(200).send({ success: true })
  })
};


exports.getLists = function (req, res) {
  registerService.list({}, (function (err, data) {
    res.status(200).json(data)
  }))
}

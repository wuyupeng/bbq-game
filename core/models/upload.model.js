var mongoose = require('mongoose');

/**
 * 提交排行
 */
var uploadSchema = new mongoose.Schema({
  nickName: {
    type: String,
  },
  rankIndex: {
    type: Number,
  },
  initEquity: {
    type: String,
  },
  currentEquity: {
    type: String,
  },
  totalPl: {
    type: String,
  },
  totalNav: {
    type: String,
  },
  returnRatioYear: {
    type: String,
  },
  returnVol: {
    type: String,
  },
  teacher: {
    type: String,
  },
  maxDrawdown: {
    type: String,
  },
  tradingDate: {
    type: String,
  },
  createAt: {
    type: String,
  },
  returnRatio: {
    type: String,
  },
  lastModifiedDate: {
    type: String,
  },
  sharpIndex: {
    type: String
  },
  points: {
    type: String
  },
  type: {
    type: String
  },
}, {
  collection: 'upload',
  id: false
});


module.exports = mongoose.model('Upload', uploadSchema);
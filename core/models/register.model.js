var mongoose = require('mongoose');

/**
 * 报名参赛
 */
var registerSchema = new mongoose.Schema({
  exchange: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  apikey: {
    type: String,
    required: true
  },
  secretkey: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  }
}, {
  collection: 'register',
  id: false
});

module.exports = mongoose.model('Register', registerSchema);
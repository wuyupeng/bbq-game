var _ = require('lodash');
var registerModel = require('../models/register.model');

/**
 * 存储报名列表
 * @param {Object} options
 * @param {Function} callback
 */
exports.save = function (options, callback) {
  if(!options.exchange || !options.name || !options.apikey || !options.secretkey || !options.address) {
    var err = {
      type: 'system',
      error: '传入参数错误'
    };
    return callback(err)
  }

  new registerModel(options).save(function (err, user) {
    if (err) {
      err.type = 'database';
      return callback(err);
    }

    callback(null)
  })
};


exports.list = function (options, callback) {
  registerModel.find({}).exec(function(err, data) {
    callback(null, data)
  })
};

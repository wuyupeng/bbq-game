var _ = require('lodash');
var uploadModel = require('../models/upload.model');

/**
 * 查询个数
 * @param {Function} callback
 */
exports.getCount = function (callback) {
  uploadModel.count({}).exec(function(err, data) {
    callback(null, data)
  })
}


/**
 * 查询排行榜
 * @param {Function} callback
 */
exports.get = function (options, callback) {
  var pageSize = options.pageSize;
  var num = (options.currentPage - 1) * pageSize;
  uploadModel.find({}).sort({
      'rankIndex': 1
    }).limit(pageSize).skip(num).exec(function (err, data) {
    callback(null, data)
  })
}
/**
 * 存储排行
 * @param {Object} data
 * @param {Function} callback
 */
exports.save = async function (data, callback) {
  if (_.size(data) < 0) {
    var err = {
      type: 'system',
      error: '传入参数错误'
    };
    return callback(err)
  }
  try {
    await uploadModel.remove({
      type: 'ranks'
    }).exec()
    await Promise.all(_.map(data, (d) => {
      return new uploadModel(d).save()
    }))
    return callback(null)
  } catch (error) {
    return callback({
      type: 'mongodb',
      error: '数据库导入失败'
    })
  }
}